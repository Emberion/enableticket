De applicatie is gebouwd in een combinatie van vue.js en laravel. 
Het betreft een single page applicatie waarvan de browser de originele pagina nooit hoeft te herladen.
De document flow die in het project is aangegeven wordt wel behouden ondanks dat de pagina een single page applicatie is.


Checklist:

Er wordt gebruik gemaakt van bootstrap

Validatie wordt via de back-end geregeld en als er iets niet klopt verschijnt er een notificatie scherm met de 
bijbehorende error in beeld. De titel is een verplicht veld.

Frameworks: er wordt gebruikt gemaakt van Laravel en Vue.js

Controlle op dezelfde naam: er wordt gekeken of er al tickets met dezelfde naam bestaan. Er verschijnt alleen een 
bevestiging scherm als een ticket met deze titel bestaat en deze ticket nog niet is afgesloten.

Logging: er wordt gebruik gemaakt van de logging functionaliteit van Laravel. In config/logging.php heb ik een custom 
channel gemaakt zodat de custom logs in een andere file genaamd storage/logs/custom.log terechtkomen.

Uitgevoerde tickets: Tickets worden uit de tickets lijst gehaald maar niet uit de database en er wordt een timestamp aan de ticket toegevoegd als
deze wordt afgesloten.

GIT: De code staat op git.

Database: Je kunt eenvoudig via migrations de benodigde tables genereren.

Custom pagination: Er is custom pagination toegevoegd. Er worden maximaal 5 tickets per pagina weergegeven. Gebruik de 
custom pagination om de volgende reeks tickets weer te geven.


Voor vue.js:

De PageLoader.vue component staat centraal aan alle andere vue.js componenten. Veel andere vue.js componenten zijn afhankelijk van 
methods en data vanuit dit component. Deze worden via properties doorgegeven aan de aangewezen componenten.


Installatie:

Pull code van git.

composer install

npm install

npm run dev

maak een kopie van .env.example en vul je db credentials in

php artisan migrate

Misschien benodigd: php artisan key:generate
