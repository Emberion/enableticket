<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Exception as Exception;

class LoggingController extends Controller
{
    /**
     * Method die exceptions logt in de /storage/logs/custom.log file
     * @param $exception
     * @return mixed
     */
    public function logException($exception){
        try{
            throw $exception;//new Exception("testing");
        }catch(Exception $e) {
            Log::channel("custom")->info($e->getMessage());
        }
    }

    /**
     * Method die berichten logt in de /storage/logs/custom.log file
     * @param $message
     * @return void
     */
    public function logMessage($message){
        Log::channel("custom")->info($message);
    }
}
