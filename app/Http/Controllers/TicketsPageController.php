<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Carbon\Carbon;
use \Exception as Exception;


class TicketsPageController extends SecurityAndValidationController
{
    /**
     * Method die tickets in pagination format inlaad. De JSON response kan in Tickets.vue gebruikt worden om een custom pagination te maken.
     * Dit was nodig aangezien de laravel-vue pagination niet goed werkte en heb ik er dus zelf 1 gemaakt.
     * Deze method laadt ook alleen de tickets in die niet zijn opgelost.
     * @return \Illuminate\Http\JsonResponse
     */
    public function grabTickets(){
        $ticketsPagination = Ticket::where("solved","=","0")->orderBy("id","DESC")->paginate(5);
        return response()->json($ticketsPagination);
    }

    /**
     * laad de gewone tickets pagina. pakt de eerste pagina van de tickets pagination in grabTickets
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function loadTicketsPage(){
        return view("tickets")->with("data",["ticketsPaginate" => $this->grabTickets()]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createTicket(){
        $requiredInputs = ["title","firstSubmit"];
        $postData = request()->post();
        //valideerd of alle inputs zijn ingesteld en beschermd de inputs tegen XSS. Zie SecurityAndValidationController voor meer info
        $securedInputs = $this->execute($postData,$requiredInputs);
        //kijkt of de ticket bestaat zo ja return dan de ticket, zo niet return dan false en ga dan gewoon verder dit is voor de controle op tickets met dezelfde naam
        $duplicateCheck = $this->checkForDublicateTicket($securedInputs["title"]);
        if($duplicateCheck !== false && $postData["firstSubmit"] === true){
            return $duplicateCheck;
        }
        $ticket = new Ticket;
        $ticket->title = strtolower($securedInputs["title"]);
        $ticket->content = $securedInputs["content"];
        $ticket->solved = 0;
        $ticket->save();
        ////log een bericht in storage/logs/custom.log zie de LoggingController voor meer info
        $this->logMessage(sprintf("Ticket with the name %s created",$securedInputs["title"]));
        return $this->loadTicketsPage();
    }

    /**
     * Method die de ticket op solved zet en de timestamp opslaat van wanneer deze ticket is opgelost.
     * @return int
     * @throws Exception
     */
    public function updateTicket(){
        $requiredInputs = ["id"];
        $postData = request()->post();
        $securedInputs = $this->execute($postData,$requiredInputs);
        $ticketId = $securedInputs["id"];
        $ticket = Ticket::where("id","=","$ticketId")->first();
        $this->checkIfRowExists($ticket);
        $ticket->solved = 1;
        $ticket->solved_at = Carbon::now();
        $ticket->save();
        //log een bericht in storage/logs/custom.log zie de LoggingController voor meer info
        $this->logMessage(sprintf("Ticket met de titel %s is afgesloten.",$ticket->title));
        return 200;
    }

    /**
     * Method die de create ticket form via blade inlaad
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createTicketForm(){
        return view("create-ticket");
    }

    /**
     * Method die de ticket uit de db ophaalt en de view laat zien. Stuurt de ticket data mee met de view rendering
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws Exception
     */
    public function showTicket($id){
        $requiredInputs = ["id"];
        //valideerd of alle inputs zijn ingesteld en beschermd de inputs tegen XSS. Zie SecurityAndValidationController voor meer info
        $securedInputs = $this->execute(["id" => $id],$requiredInputs);
        $ticket = Ticket::where("id","=",$securedInputs["id"])->first();
        $this->checkIfRowExists($ticket);
        return view("show-ticket")->with("data",["ticket" => $ticket]);
    }
}
