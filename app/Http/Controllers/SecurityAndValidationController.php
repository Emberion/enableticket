<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Exception as Exception;
use App\Models\Ticket;

class SecurityAndValidationController extends LoggingController
{
    /**
     * Method die kijkt of een gekozen row bestaat. Zoniet dan bestaat de ticket niet. Dit wordt gebruikt omdat er veel ->first() wordt gebruikt binnen de controllers.
     * @param $row
     * @return void
     * @throws Exception
     */
    public function checkIfRowExists($row){
        if(empty($row)){
            $exception = new Exception("No such ticket exists");
            //log de exception in storage/logs/custom.log zie de LoggingController voor meer info
            $this->logException($exception);
            throw $exception;
        }
    }

    /**
     * method die kijkt of een ticket al bestaat. Als de ticket niet bestaat return false, als de ticket bestaat return dan de ticket data zodat deze in vue kan worden gebruikt bij het weet je het zeker scherm.
     * @param $ticketName
     * @return false
     */
    public function checkForDublicateTicket($ticketName){
        $ticketName = strtolower($ticketName);
        $ticket = Ticket::where("title","=","$ticketName")->where("solved","=","0")->first();
        if(!empty($ticket)){
            return $ticket;
        }
        return false;
    }

    /**
     * Method die kijkt of alle required input fields zijn ingesteld en kijkt of de input fields geen lege string zijn.
     * @param $requestData
     * @param $requiredInputs
     * @return bool or string
     */
    public function checkIfRequiredInputsSet($requestData,$requiredInputs){
        foreach($requiredInputs as $input){
            if(empty($requestData[$input])){
                return $input;
            }
        }
        return true;
    }

    /**
     * Method die de inputs tegen XSS beschermt
     * @param $requestData
     * @return array
     */
    public function protectAgainstXSS($requestData){
        $securedInputs = [];
        foreach($requestData as $name => $input){
            $securedInput = htmlspecialchars($input);
            $securedInput = strip_tags($securedInput);
            $securedInputs[$name] = $securedInput;
        }
        return $securedInputs;
    }

    /**
     * Method die alle bovenstaande methods in 1 call kan oproepen. Gebaseerd op welke params je invult.
     * @param $requestData
     * @param $requiredInputs
     * @param $row
     * @return array
     * @throws Exception
     */
    public function execute($requestData,$requiredInputs,$row=null){
        if(isset($row)){
            $this->checkIfRowExists($row);
        }
        $requiredInputsResult = $this->checkIfRequiredInputsSet($requestData,$requiredInputs);
        if($requiredInputsResult !== true){
            $exception = new Exception(sprintf("Required input %s not set.",$requiredInputsResult));
            //log de exception in storage/logs/custom.log zie de LoggingController voor meer info
            $this->logException($exception);
            throw $exception;
        }
        return $this->protectAgainstXSS($requestData);
    }
}
