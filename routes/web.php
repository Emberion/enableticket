<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketsPageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/",[TicketsPageController::class,"loadTicketsPage"])->name("index");

Route::prefix("tickets")->group(function(){
    route::get("/show",[TicketsPageController::class,"grabTickets"])->name("tickets");
    route::get("/create_ticket",[TicketsPageController::class,"createTicketForm"])->name("ticketForm");
    route::post("/submit_ticket",[TicketsPageController::class,"createTicket"])->name("submitTicket");
    route::post("/update_ticket",[TicketsPageController::class,"updateTicket"])->name("updateTicket");
    route::get("/show/{id}",[TicketsPageController::class,"showTicket"])->name("showTicket");
});
